import numpy as np
import cv2
import torch
import os
import torchvision.transforms as transforms
import torchvision.models as models
import urllib.request
from PIL import Image

# Extract pre-trained face detector
face_cascade = cv2.CascadeClassifier('haarcascades/haarcascade_frontalface_alt.xml')

# Load the models
dogs = models.vgg16(pretrained=True)
model = torch.load('model.pt').cpu()

# Dictionary of classes
classes = np.load('classes.npy', allow_pickle=True).item()


def convert_to_RGB(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


def predict_class(img_path, pre_trained_model):
    # Use pre-trained model to obtain index corresponding to
    # predicted ImageNet class for image at specified path

    # Returns:
    #    Index corresponding to pre-trained model's prediction

    # Load and pre-process an image from the given img_path
    # Return the *index* of the predicted class for that image

    # Get the image
    image = get_image(img_path)
    output = pre_trained_model(image)

    _, preds_tensor = torch.max(output, 1)
    predicted_class_index = np.squeeze(preds_tensor.numpy())

    return predicted_class_index


# Returns "True" if face is detected in image stored at img_path
def face_detector(img_path, detector):
    urllib.request.urlretrieve(img_path, "temp")
    img = cv2.imread("temp")
    os.remove("temp")
    gray = convert_to_RGB(img)
    faces = detector.detectMultiScale(gray)
    return len(faces) > 0


# Returns "True" if a dog is detected in the image stored at img_path
def dog_detector(img_path, pre_trained_model):
    is_dog = False
    index = predict_class(img_path, pre_trained_model)

    if (index >= 151) & (index <= 268):
        is_dog = True

    return is_dog


def get_image(img_path):
    # According to the link (https://pytorch.org/docs/stable/torchvision/models.html), all pre-trained models expect
    # input images normalized in the same way, i.e. mini-batches of 3-channel RGB images of shape (3 x H x W),
    # where H and W are expected to be at least 224. The images have to be loaded in to a range of [0, 1] and then
    # normalized using mean = [0.485, 0.456, 0.406] and std = [0.229, 0.224, 0.225].

    urllib.request.urlretrieve(img_path, "temp")
    image = Image.open("temp").convert('RGB')
    os.remove("temp")

    transformations = transforms.Compose([transforms.Resize(size=224),
                                          transforms.CenterCrop((224, 224)),
                                          transforms.ToTensor(),
                                          transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                               std=[0.229, 0.224, 0.225])])
    image = transformations(image)[:3, :, :].unsqueeze(0)

    return image


def predict_breed_transfer(img_path, model_used):
    # load the image and return the predicted breed

    # list of class names by index, i.e. a name can be accessed like class_names[0]
    class_names = [item[4:].replace("_", " ") for item in classes['train'].classes]
    index = predict_class(img_path, model_used)

    return class_names[index]


def run_predictions(img_path):
    # Handle cases for a human face, dog, and neither

    color = 'blue'
    background = '#e6f7ff'
    # Check if we found a dog in the picture using VGG16 pre-trained model
    if dog_detector(img_path, dogs):
        msg = "Hello, I see a dog! "
        msg += "It looks like a [ {} ].".format(predict_breed_transfer(img_path, model).upper())

    # Check if we found a human in the picture using OpenCV
    elif face_detector(img_path, face_cascade):
        msg = "Hello, I see a human! "
        msg += "This human looks like a [ {} ].".format(predict_breed_transfer(img_path, model).upper())

    # No humans... no dogs...
    else:
        msg = "Ooops... Something is wrong! "
        msg += "I found neither dogs nor humans. Please, give me another picture to analyse."
        color = 'red'
        background = '#ffe6e6'

    return msg, color, background
